var gulp = require("gulp"),
    cleanCSS = require("gulp-clean-css"),
    uglify = require("gulp-uglify"),
    imagemin = require("gulp-imagemin"),
    ngAnnotate = require("gulp-ng-annotate"),
    rename = require("gulp-rename"),
    sass = require("gulp-sass"),
    autoprefixer = require("gulp-autoprefixer"),
    webserver = require("gulp-webserver"),
    livereload = require("gulp-livereload"),
    babel = require("gulp-babel"),
    plumber = require("gulp-plumber"),
    changed = require("gulp-changed"),
    watch = require("gulp-watch"),
    concat = require("gulp-concat"),
    del = require("del"),
    path = require("path"),
    Q = require("q");

//获取编译时设置的环境变量，去除变量字符串的前后空格，方便字符串比较
var BUILD_ENV, isProd;
if (process.env.BUILD_ENV) {
    BUILD_ENV = process.env.BUILD_ENV.replace(/(^\s*)|(\s*$)/g, "");
}

//判断当前执行命令设置的环境
isProd = BUILD_ENV === "prod";

//路径配置
const appPaths = {
    output: ["build"],
    src: {
        styles: {
            //index: ["src/**/*.{css,scss}"]
            disasterManage: [
                "src/modules/disasterManage/styles/disasterList.scss",
                "src/modules/disasterManage/styles/disasterPoint.scss"
            ]
        },
        download: ["src/download/**/*.*"],
        assets: ["src/**/*.{png,jpg,gif,jpeg,ico}"],
        scripts: [
            "src/**/*.js",
            "!src/libs/**/*.js",
            "!src/require.config.js",
            "!src/web.cfg.js"
        ],
        libs: ["src/libs/**/*.*", "src/libs/**/*.scss"],
        views: ["src/**/*.html", "!src/libs/**/*.html"],
        others: ["src/require.config.js", "src/web.cfg.js"]
    }
};

//拷贝下载附件
gulp.task("downloadcopy", function () {
    return gulp.src(appPaths.src.download)
        .pipe(changed(path.join("build", "download")))
        .pipe(gulp.dest(path.join("build", "download")));
});

//拷贝lib
gulp.task("libcopy", function () {
    return gulp.src([appPaths.src.libs[0], "!src/libs/**/*.scss"])
        .pipe(changed(path.join("build", "libs")))
        .pipe(gulp.dest(path.join("build", "libs")));
});

//编译lib中的sass文件
gulp.task("libstylecompile", function () {
    if (isProd) {
        return gulp.src(appPaths.src.libs[1])
            .pipe(sass({
                outputStyle: "compressed"
            }).on("error", function (error) {
                console.error(error.toString());
                this.emit("end");
            }))
            .pipe(autoprefixer())
            .pipe(cleanCSS())
            .pipe(gulp.dest(path.join("build", "libs")));
    } else {
        return gulp.src(appPaths.src.libs[1])
            .pipe(changed(path.join("build", "libs")))
            .pipe(sass({
                outputStyle: "expanded"
            }).on("error", function (error) {
                console.error(error.toString());
                this.emit("end");
            }))
            .pipe(autoprefixer())
            .pipe(gulp.dest(path.join("build", "libs")));
    }
});

//拷贝page
gulp.task("pagecopy", function () {
    return gulp.src(appPaths.src.views)
        .pipe(changed("build"))
        .pipe(gulp.dest("build"));
});

//拷贝其它文件
gulp.task("otherscopy", function () {
    return gulp.src(appPaths.src.others)
        .pipe(changed("build"))
        .pipe(gulp.dest("build"));
});

//配置文件处理
gulp.task("appconfigcopy", function () {
    var configPath;
    if (isProd) {
        configPath = "site/web.cfg.release.js";
    } else {
        configPath = "site/web.cfg.js";
    }

    return gulp.src(configPath)
        .pipe(rename("web.cfg.js"))
        .pipe(gulp.dest("build"));
});

//压缩style
gulp.task("stylecompile", function () {
    var deferred = Q.defer();
    setTimeout(function () {
        var styles = appPaths.src.styles;
        for (attr in styles) {
            if (styles.hasOwnProperty(attr)) {
                gulp.src(appPaths.src.styles[attr])
                    .pipe(sass({
                        outputStyle: isProd ? "compressed" : "expanded"
                    }).on("error", function (error) {
                        console.error(error.toString());
                        this.emit("end");
                    }))
                    .pipe(autoprefixer())
                    .pipe(concat(attr + ".min.css"))
                    .pipe(cleanCSS())
                    .pipe(gulp.dest(path.join("build", "modules/" + attr + "/styles")));
            }
        }
        deferred.resolve();
    }, 1);
    return deferred.promise;
});

//压缩图片
gulp.task("imagemin", function () {
    if (isProd) {
        return gulp.src(appPaths.src.assets)
            .pipe(imagemin({
                optimizationLevel: 5, //类型：Number  默认：3  取值范围：0-7（优化等级）  
                progressive: true, //类型：Boolean 默认：false 无损压缩jpg图片  
                interlaced: true, //类型：Boolean 默认：false 隔行扫描gif进行渲染  
                multipass: true //类型：Boolean 默认：false 多次优化svg直到完全优化  
            }))
            .pipe(gulp.dest("build"));
    } else {
        return gulp.src(appPaths.src.assets)
            .pipe(plumber({}, true, function (err) {
                console.log(err);
            }))
            .pipe(changed("build"))
            .pipe(gulp.dest("build"));
    }
});

//压缩js脚本
gulp.task("scriptmin", function () {
    //开发环境和发布环境分别做不同处理
    if (isProd) {
        return gulp.src(appPaths.src.scripts)
            .pipe(ngAnnotate({
                single_quotes: true
            }))
            .pipe(babel({
                presets: ["env"]
            }))
            .pipe(uglify())
            .pipe(gulp.dest("build"));
    } else {
        return gulp.src(appPaths.src.scripts)
            .pipe(plumber({}, true, function (err) {
                console.log(err);
            }))
            .pipe(changed("build"))
            .pipe(ngAnnotate({
                single_quotes: true
            }))
            .pipe(babel({
                presets: ["env"]
            }))
            .pipe(gulp.dest("build"));
    }
});

//清除上次编译的文件
gulp.task("cleandir", function () {
    var deferred = Q.defer();
    del(appPaths.output).then(paths => {
        console.log("Deleted files and folders:\n", paths.join("\n"));
        deferred.resolve();
    });
    return deferred.promise;
});

//编译项目
gulp.task("build", ["cleandir"], function () {
    gulp.start("downloadcopy", "libcopy", "libstylecompile", "pagecopy",
        "otherscopy", "appconfigcopy", "stylecompile", "imagemin", "scriptmin");
});

//监视文件
gulp.task("filewatch", function () {
    var deferred = Q.defer();
    // 执行异步的操作
    setTimeout(function () {
        gulp.watch(appPaths.src.views, ["pagecopy"]);
        gulp.watch(appPaths.src.styles.index, ["stylecompile"]);
        gulp.watch(appPaths.src.others, ["otherscopy"]);
        gulp.watch("src/web.cfg.js", ["appconfigcopy"]);
        gulp.watch(appPaths.src.assets, ["imagemin"]);
        gulp.watch(appPaths.src.scripts, ["scriptmin"]);
        gulp.watch(appPaths.src.libs[1], ["libstylecompile"]);
        gulp.watch(["src/libs/**/*.{css,js,html}"], ["libcopy"]);
        deferred.resolve();
    }, 1);
    return deferred.promise;
});

//启动webserver
gulp.task("startserver", ["filewatch"], function () {
    gulp.src("./build")
        .pipe(webserver({
            //实时刷新
            livereload: {
                enable: true,
                filter: function (fileName) {
                    return !fileName.match(/.map$/);
                }
            },
            directoryListing: false,
            port: 4002,
            host: "192.168.130.29",
            //访问的路径是否显示
            //open: "http://192.168.5.24:4002/views/index.html"
            open: "http://192.168.130.29:4002/bootstraps/index/index.html"
        }));
});