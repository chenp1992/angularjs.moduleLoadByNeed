define([], function (moment) {

    "use strict";

    //基础设施模块
    var infrastructureModule = angular.module("infrastructureModule", []);
    return infrastructureModule;
});