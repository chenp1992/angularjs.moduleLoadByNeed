define(["infrastructureModule",
    "infrastructure/scripts/services/httpService",
    "infrastructure/scripts/services/messagerService",
    "infrastructure/scripts/services/dialogService",
    "infrastructure/scripts/directives/commonDirective"
], function (infrastructureModule) {
    return infrastructureModule;
});