define(["infrastructureModule"], function (infrastructureModule) {

    "use strict";

    //提示框服务
    infrastructureModule.controller("messagerController", messagerController)
        .factory("messager", messagerService)
        .constant("MessagerIcon", {
            None: 0,
            Information: 1,
            StopSign: 2,
            Exclamation: 3,
            Question: 4,
            Error: 5,
            Success: 6
        })
        .constant("MessagerButton", {
            OK: 0,
            Cancel: 1,
            OKCancel: 2,
            YesNo: 3,
            YesNoCancel: 4,
            OKCancelApply: 5,
            RetryCancel: 6,
            AbortRetryIgnore: 7
        });

    function messagerController($scope, $modalInstance, $timeout, $sce, MessagerIcon, MessagerButton, values) {
        function isFunction(fnName) {
            return values.fns && typeof values.fns[fnName] === "function";
        }

        $scope.ok = function () {
            if (isFunction("okFn"))
                values.fns.okFn();
            //close的参数是向$modal.open方法的返回值modal.result.then方法中的function参数传递的数据
            $modalInstance.close();
        };

        $scope.yes = function () {
            if (isFunction("yesFn"))
                values.fns.yesFn();
            $modalInstance.close("yes");
        }

        $scope.no = function () {
            if (isFunction("noFn"))
                values.fns.noFn();
            $modalInstance.close("no");
        }

        $scope.cancel = function () {
            if (isFunction("cancelFn"))
                values.fns.cancelFn();
            $modalInstance.dismiss();
        }

        $scope.modal = {};
        $scope.modal.Title = values.title;

        // $sce.trustAsHtml 需要传入string类型参数
        if (values.msg && typeof values.msg !== "string")
            values.msg = values.msg.toString();
        $scope.modal.Text = $sce.trustAsHtml(values.msg);

        switch (values.icon) {
            case MessagerIcon.Information:
                $scope.modal.Icon = "/assets/alert.png";
                break;
            case MessagerIcon.Question:
                $scope.modal.Icon = "/assets/confirm.png";
                break;
            case MessagerIcon.Error:
                $scope.modal.Icon = "/assets/error.png";
                break;
            case MessagerIcon.None:
                break;
        }

        $scope.show = {
            Btns: false,
            Ok: false,
            Yes: false,
            No: false,
            Cancel: false
        }

        switch (values.btns) {
            case MessagerButton.OK:
                $scope.show.Ok = true;
                break;
            case MessagerButton.OKCancel:
                $scope.show.Ok = true;
                $scope.show.Cancel = true;
                break;
            case MessagerButton.YesNoCancel:
                $scope.show.Yes = true;
                $scope.show.No = true;
                $scope.show.Cancel = true;
                break;
        }

        if (values.autoClose) {
            $timeout(function () {
                $modalInstance.dismiss();

                //自动关闭时如果传入回调方法执行回调
                if ($scope.ok && typeof $scope.ok === "function") {
                    $scope.ok();
                }
            }, values.millisec || 1200);
        } else {
            $scope.show.Btns = true;
        }
    }

    function showMessager(modal, title, msg, millisec, icon, btns, fns) {
        var autoClose = millisec !== 0;

        // var id = new Date().getTime();       

        // resetMe({
        //     width: "400px",
        // }, id);

        // function resetMe(obj, id) {
        //     init(obj, id);
        // }

        // function init(obj, id) {
        //     setTimeout(function () {
        //         var modals = $(".modal-"+id);
        //         if (modals.length) {
        //             modals.css("width", obj.width);
        //         }
        //         else {
        //             init(obj, id);
        //         }
        //     }, 30);
        // }

        return modal.open({
            templateUrl: "../../../views/common/messager.html",
            backdrop: "static",
            controller: "messagerController",
            size: "sm",
            resolve: {
                values: function () {
                    return {
                        title: title,
                        msg: msg,
                        millisec: millisec,
                        autoClose: autoClose,
                        icon: icon,
                        btns: btns,
                        fns: fns,
                    };
                }
            }
        });
    }

    function messagerService($modal, MessagerIcon, MessagerButton) {
        var messager = {
            /**
             * alert方法
             * @param {String} msg 提示信息
             * @param {Number} millisec 提示框显示时间（默认1.2秒，为0时代表不自动关闭）
             */
            alert: function (msg, millisec, okFn) {
                if (okFn && typeof okFn === "function") {
                    var fns = { okFn: okFn };
                    showMessager($modal, "提示信息", msg, millisec, MessagerIcon.Information, MessagerButton.OK, fns);
                } else {
                    showMessager($modal, "提示信息", msg, millisec, MessagerIcon.Information, MessagerButton.OK);
                }
            },
            /**
             * confirm方法
             * @param {String} msg 提示信息
             * @param {Function} okFn 点击确定时的回调函数
             * @param {Function} cancelFn 点击取消时的回调函数
             */
            confirm: function (msg, okFn, cancelFn) {
                var fns = { okFn: okFn, cancelFn: cancelFn };
                showMessager($modal, "提示信息", msg, 0, MessagerIcon.Question, MessagerButton.OKCancel, fns)
                    .result.then(function (info) { }, function (info) {
                        // 按退出键退出时，也需执行cancelFn
                        if (info == "escape key press" && typeof cancelFn === "function") {
                            cancelFn();
                        }
                    });
            },
            /**
             * error方法
             * @param {String} msg 提示信息
             * @param {Number} millisec 提示框显示时间（默认1.2秒，为0时代表不自动关闭）
             */
            error: function (msg, millisec) {
                showMessager($modal, "警告", msg, millisec, MessagerIcon.Error, MessagerButton.OK);
            },
            /**
             * yesNoConfirm方法
             * @param {String} msg 提示信息
             * @param {Function} yesFn 点击是时的回调函数
             * @param {Function} noFn 点击否时的回调函数
             * @param {Function} cancelFn 点击取消时的回调函数
             */
            yesNoConfirm: function (msg, yesFn, noFn, cancelFn) {
                var fns = { yesFn: yesFn, noFn: noFn, cancelFn: cancelFn };
                showMessager($modal, "提示信息", msg, 0, MessagerIcon.Question, MessagerButton.YesNoCancel, fns)
                    .result.then(function (info) { }, function (info) {
                        // 按退出键退出时，也需执行cancelFn
                        if (info == "escape key press" && typeof cancelFn === "function") {
                            cancelFn();
                        }
                    });
            }
        };
        return messager;
    }
});