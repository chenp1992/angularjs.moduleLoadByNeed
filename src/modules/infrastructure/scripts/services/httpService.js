define(["infrastructureModule", "config"], function (infrastructureModule, config) {

    "use strict";

    function transformToString(Params) {
        var queryParams = Params;
        var result = "";
        for (var key in queryParams) {
            if (result == "") {
                result += "?" + key + "=" + parsedValue(queryParams[key]);
            } else {
                result += "&" + key + "=" + parsedValue(queryParams[key]);
            }
        }
        return result;

        function parsedValue(v) {
            if (typeof v == "undefined" || v === null) {
                return "";
            } else {
                return v;
            }
        }
    }

    //http服务
    infrastructureModule.factory("httpService", function ($http, $cookies, $window) {
        var authID = $cookies.get("AUTH_ID");

        var hostAddress = config.wcfAddress;

        var headers = {
            "Content-Type": "application/json; charset=UTF-8",
            "AUTH_ID": authID
        };

        function onError(ex, statusCode) {
            if (statusCode == 403) {
                //超时
                $cookies.remove("AUTH_ID");
                alert("操作超时，请重新登录！");
                $window.location.replace("/page/login.html");
            } else if (statusCode == 500) {
                //请求出错
                alert("请求出现错误！");
            }
        }

        function fn(callback) {
            return typeof callback === "function" ? callback : angular.noop;
        }

        var httpService = {
            $http: function () {
                return $http;
            },

            /**
             * 发送get请求，用于查询、获取数据
             * queryParams：附加到url中查询字符串参数的json对象
             * callback：请求成功时的回调方法
             */
            get: function (url, queryParams, callback) {
                if (url.indexOf("http") < 0) {
                    url = hostAddress + url;
                }
                $http.get(url, {
                    params: queryParams,
                    headers: headers
                }).success(fn(callback)).error(onError);
            },

            /**
             * 发送post请求，用于新增数据
             * data：提交到服务端的数据的json对象
             * queryParams：可选
             */
            post: function (url, data, callback, queryParams) {
                if (url.indexOf("http") < 0) {
                    url = hostAddress + url;
                }
                $http.post(url, data, {
                    params: typeof queryParams === "undefined" ? null : queryParams,
                    headers: headers
                }).success(fn(callback)).error(onError);
            },

            /**
             * 发送put请求，用于修改数据
             * queryParams：可选
             */
            put: function (url, data, callback, queryParams) {
                $http.put(hostAddress + url, data, {
                    params: typeof queryParams === "undefined" ? null : queryParams,
                    headers: headers
                }).success(fn(callback)).error(onError);
            },

            /**
             * 发送delete请求，用于删除数据
             */
            delete: function (url, queryParams, callback) {
                $http.delete(hostAddress + url, {
                    params: queryParams,
                    headers: headers
                }).success(fn(callback)).error(onError);
            },

            //下载需要的方法get
            downloadGet: function (url, params, callback) {
                var queryParams = angular.copy(params);
                queryParams.AUTH_ID = encodeURIComponent(authID);
                var partUrl = transformToString(queryParams);
                var resulturl = hostAddress + url + partUrl;
                window.open(resulturl);
            },

            //下载需要的方法post,这个方法会在dombody上增加隐藏元素        
            downloadPost: function (url, params, callback) {
                var queryParams = angular.copy(params);
                var resulturl = this.wrapUrl(hostAddress + url);

                formSumbit(resulturl, queryParams);

                function formSumbit(actionUrl, condition) {
                    var form = document.createElement("form");
                    form.style.display = "none";
                    form.action = actionUrl;
                    form.method = "post";
                    form.target = "_blank";
                    document.body.appendChild(form);

                    for (var attr_key in queryParams) {
                        if (queryParams.hasOwnProperty(attr_key)) {
                            var attr_value = queryParams[attr_key];
                            var cinput = createHiddenInput(attr_key, attr_value);
                            form.appendChild(cinput);
                        }
                    }

                    form.submit();
                }

                function createHiddenInput(name, value) {
                    var input = document.createElement("input");
                    input.type = "hidden";
                    input.name = name;
                    input.value = value;
                    return input;
                }
            },

            /**
             * 在url中包装身份票据
             */
            wrapUrl: function (url) {
                var p = "AUTH_ID=" + encodeURIComponent(authID);
                if (url.indexOf("?") >= 0) {
                    url += "&" + p;
                } else {
                    url += "?" + p;
                }
                return url;
            },

            /**
             * 在url中包装身份票据
             */
            wrapPDFUrl: function (url) {
                var p = "AUTH_ID=" + encodeURIComponent(authID);
                url += ";" + p;
                return url;
            },

            /**
             * 包装请求头
             */
            wrapHeader: function (header) {
                return angular.extend(header || {}, {
                    headers: {
                        "AUTH_ID": authID
                    }
                });
            }
        };

        return httpService;
    });
});