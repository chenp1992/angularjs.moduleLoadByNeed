define(["infrastructureModule"], function (infrastructureModule) {

    "use strict";

    //模态对话框服务
    infrastructureModule.factory("dialog", function ($modal, $timeout, $window) {
        function init(w, h, id, full) {
            var className = "modal-dialog modal-" + id;
            var modals = $window.document.getElementsByClassName(className);
            if (modals.length == 0) {
                $timeout(function () {
                    init(w, h, id, full);
                }, 30);
                return;
            }

            for (var index = 0; index < modals.length; index++) {
                var element = angular.element(modals[index]);
                if (full) {
                    element.css({
                        "width": "100%",
                        "height": "100%",
                        "margin": 0
                    });
                    element.children(0).css({
                        "width": "100%",
                        "height": "100%",
                        "background": "transparent",
                        "overflow": "hidden",
                        "border": "none",
                        "border-radius": 0
                    });
                } else {
                    if (w > 0)
                        element.css("width", w + "px");
                    if (h > 0)
                        element.css("height", h + "px");
                }
            }
        }

        var dialog = {
            /**
             * 打开一个模态对话框
             * @param {String} templateUrl 同$modal
             * @param {String} template 同$modal
             * @param {String|Function} controller 同$modal
             * @param {Object} resolve 同$modal
             * @param {String} backdrop 同$modal
             * @param {Boolean} animation 同$modal
             * @param {Number} width 模态框的宽度（整数不带px）（可选）
             * @param {Number} height 模态框的高度（整数不带px）（可选）
             * @param {Boolean} full 模态框是否全屏显示（可选）
             * @return {Object} 模态框对象
             */
            open: function (param) {
                var full = typeof param.full == "boolean" ? param.full : false;
                var w = typeof param.width === "number" ? param.width : 0;
                var h = typeof param.height === "number" ? param.height : 0;
                var backdrop = param.backdrop !== void 0 && param.backdrop !== null ? param.backdrop : true;
                var animation = typeof param.animation == "boolean" ? param.animation : true;
                var id = new Date().getTime();//用作唯一标示
                var modal = $modal.open({
                    templateUrl: param.templateUrl,
                    template: param.template,
                    backdrop: backdrop,
                    controller: param.controller,
                    resolve: param.resolve,
                    size: id,
                    animation: animation
                });
                modal.opened.then(function () {
                    $timeout(function () {
                        init(w, h, id, full);
                    }, 30);
                });
                return modal;
            }
        };
        return dialog;
    });


    //打开模态对话框
    infrastructureModule.factory("modal", function(dialog) {
        /**
         * 打开一个模态框
         * @param {String} templateUrl 页面Url地址 
         * @param {Number|Boolean} width 模态框宽度
         * @param {String|Function} controller 控制器名称/方法
         * @param {Object} values 模态框参数，如：{ values: { } }
         * @param {Boolean} animation 是否显示动画
         * @returns {Object} 模态框对象
         */
         function openModal(templateUrl, width, controller, values, animation) {
            var config = {
                templateUrl: templateUrl,
                backdrop: "static",
                controller: controller,
                animation: animation,
                resolve: {}
            };
            typeof width == "boolean" ? config.full = width : config.width = width;
            if (values && typeof values == "object") {
                for (var v in values) {
                    config.resolve[v] = (function (v) {
                        return function () { return values[v]; }
                    })(v);
                }
            }
            var modal = dialog.open(config)
            return modal;
        }
        return {
            open: openModal
        };
    }); 
});