define(["infrastructureModule"], function (infrastructureModule) {
    infrastructureModule.directive("draggable", function ($window, $document) {
        return function (scope, element, attr) {
            var startX = 0,
                startY = 0,
                x = 0,
                y = 0;
            var header = angular.element(element[0]);

            var target = element[0];
            while (angular.element(target).attr('class') != 'modal-content') {
                target = target.parentNode;
            }
            var modal = angular.element(target);

            var modalBox = angular.element(document.getElementsByClassName("modal"));

            modalBox.css({
                overflow: 'hidden'
            }); //隐藏模态框外层滚动条

            header.on('mousedown', mousedown);

            function mousedown(e) {
                e.preventDefault();
                header.css({
                    cursor: 'move'
                });
                startX = e.pageX - x;
                startY = e.pageY - y;
                $document.on('mousemove', mousemove);
                $document.on('mouseup', mouseup);
            }

            function mousemove(e) {
                y = e.pageY - startY;
                x = e.pageX - startX;

                boundControl();
                modal.css({
                    top: y + 'px',
                    left: x + 'px'
                });
            }

            // 边界控制，防止模态框被移到屏幕外
            function boundControl() {
                if (x < -(modal[0].offsetParent.offsetLeft + modal[0].offsetWidth - 50))
                    x = -(modal[0].offsetParent.offsetLeft + modal[0].offsetWidth - 50)
                else if (x > modal[0].offsetParent.offsetLeft + modal[0].offsetWidth - 50)
                    x = modal[0].offsetParent.offsetLeft + modal[0].offsetWidth - 50
                if (y < -modal[0].offsetParent.offsetTop)
                    y = -modal[0].offsetParent.offsetTop;
                else if (y > $window.innerHeight - modal[0].offsetParent.offsetTop - 40)
                    y = $window.innerHeight - modal[0].offsetParent.offsetTop - 40;
            }

            function mouseup(e) {
                header.css({
                    cursor: 'default'
                });
                $document.off('mousemove', mousemove);
                $document.off('mouseup', mouseup);
            }
        };
    })
})