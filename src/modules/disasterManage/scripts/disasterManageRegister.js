define(["disasterManageModule",
    "disasterManage/scripts/controllers/disasterListController",
    "disasterManage/scripts/services/disasterManageService"
], function (disasterManageModule) {
    return disasterManageModule;
});