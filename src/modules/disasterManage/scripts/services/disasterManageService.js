define(["disasterManageModule", "config"], function (disasterManageModule, config) {

    "use strict";

    //地灾管理模块服务
    disasterManageModule.factory("disasterManageService", function (httpService) {
        return {
            "queryList": function (param, callback) {
                httpService.post("/earthquake/list", param, callback);
            },

            "add": function (param, callback) {
                httpService.post("/earthquake/add", param, callback);
            },

            "delete": function (param, callback) {
                httpService.delete("/earthquake/delete?id=" + param, null, callback);
            },

            "select": function (param, callback) {
                httpService.get("/earthquake/select?id=" + param, null, callback);
            },

            "update": function (param, callback) {
                httpService.post("/earthquake/update", param, callback);
            }
        };
    });
});