define([], function (moment) {

    "use strict";

    //地灾管理模块
    var disasterManageModule = angular.module("disasterManageModule", []);
    return disasterManageModule;
});