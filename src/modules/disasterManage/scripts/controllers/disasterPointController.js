define([
    "disasterManageModule",
    "disasterManage/scripts/services/disasterManageService"
], function (disasterManageModule) {
    "use strict";

    disasterManageModule.controller("disasterPointController", function ($scope, $modalInstance, $q,
        messager, disasterManageService, values) {

        $scope.enumData = {
            "disasterTypes": [{
                    "value": 1,
                    "text": "滑坡"
                },
                {
                    "value": 2,
                    "text": "泥石流"
                },
                {
                    "value": 3,
                    "text": "崩塌"
                }
            ],
            "scaleLevels": [{
                    "value": 1,
                    "text": "小型"
                },
                {
                    "value": 2,
                    "text": "中型"
                },
                {
                    "value": 3,
                    "text": "大型"
                }
            ],
            "stabilitys": [{
                    "value": 1,
                    "text": "低易发"
                },
                {
                    "value": 2,
                    "text": "中易发"
                },
                {
                    "value": 3,
                    "text": "较差"
                },
                {
                    "value": 4,
                    "text": "差"
                }
            ],
            "disasterTargets": [{
                    "value": 1,
                    "text": "居民房屋"
                },
                {
                    "value": 2,
                    "text": "教育设施"
                },
                {
                    "value": 3,
                    "text": "公路交通"
                },
                {
                    "value": 4,
                    "text": "工业厂房"
                }, {
                    "value": 5,
                    "text": "宗教场所"
                },
                {
                    "value": 6,
                    "text": "活动场所"
                },
                {
                    "value": 7,
                    "text": "宾馆酒店"
                },
                {
                    "value": 8,
                    "text": "旅游景点"
                },
                {
                    "value": 9,
                    "text": "其它设施"
                },
                {
                    "value": 10,
                    "text": "居民房屋+教育设施"
                }
            ],
            "harmDegrees": [{
                    "value": 1,
                    "text": "一般级"
                },
                {
                    "value": 2,
                    "text": "较大级"
                },
                {
                    "value": 3,
                    "text": "重大级"
                }
            ],
            "dangerTypes": [{
                    "value": 1,
                    "text": "小"
                },
                {
                    "value": 2,
                    "text": "中等"
                },
                {
                    "value": 3,
                    "text": "大"
                }
            ],
            "preventMeasures": [{
                    "value": 1,
                    "text": "搬迁避让"
                },
                {
                    "value": 2,
                    "text": "群测群防"
                },
                {
                    "value": 3,
                    "text": "工程治理"
                },
                {
                    "value": 4,
                    "text": "搬迁或治理"
                }
            ]
        };

        $scope.disasterPoint = {
            "affectedObject": 1,
            "danger": 1,
            "disasterType": 1,
            "eastlongitude": "",
            "firstOccurrenceTime": "",
            "harmDegree": 1,
            "harmMeasures": 1,
            "houseHolds": 0,
            "isDesign": 0,
            "isGoverned": 0,
            "isGovernning": 0,
            "isProinvestigation": 0,
            "isRelocated": 0,
            "isSimplydesign": 0,
            "isSurvey": 0,
            "location": "",
            "no": "",
            "northlatitude": "",
            "realHouseHolds": 0,
            "realPopulation": 0,
            "scale": 0,
            "scaleLevel": 1,
            "stabilityType": 1,
            "threateneDassets": 0,
            "threatenedPopulation": 0,
            "townStreet": ""
        };

        //标识是否正在进行操作
        var isBusy = false;

        //控制界面元素可用性
        $scope.disabled = {
            "no": false,
            "save": true,
            "cancel": true
        };

        //关闭
        $scope.close = function () {
            $modalInstance.dismiss();
        };

        //根据Id获取地灾点数据
        function getDataById(id) {
            var deferred = $q.defer();
            //请求数据
            disasterManageService.select(id, function (data) {
                if (data && data.resultStatus === "SUCCESS") {
                    deferred.resolve(data.data);
                } else {
                    deferred.reject(data.message);
                }
            });
            return deferred.promise;
        }

        //编辑状态根据id查询
        var isUpdate = false;
        if (values && values.id) {
            isBusy = true;
            isUpdate = true;
            $scope.title = "编辑地灾隐患点";
            getDataById(values.id).then(function (data) {
                if (data) {
                    $scope.disasterPoint = data;
                }
                $scope.disabled.no = true;
                $scope.disabled.save = false;
                $scope.disabled.cancel = false;
                isBusy = false;
            }, function () {
                isBusy = false;
                messager.alert("地灾隐患点不存在，无法进行编辑！", 0);
                return false;
            });
        } else {
            $scope.title = "添加地灾隐患点";
            $scope.disabled.save = false;
            $scope.disabled.cancel = false;
        }

        //验证表单输入
        function validateBaseInfo() {
            //验证输入
            if ($scope.baseInfoForm.no.$invalid) {
                messager.alert("请输入地灾隐患点编号！", 0);
                return false;
            }
            if ($scope.baseInfoForm.townStreet.$invalid) {
                messager.alert("请输入地灾隐患点所在街镇！", 0);
                return false;
            }
            if ($scope.baseInfoForm.location.$invalid) {
                messager.alert("请输入地灾隐患点位置！", 0);
                return false;
            }
            return true;
        }

        //保存回调
        function saveCallback(data) {
            if (data && data.resultStatus === "SUCCESS") {
                messager.alert("保存成功！", 1800);
                $modalInstance.close();
            } else {
                messager.alert("保存失败：" + data.message + "！");
            }
        }

        //保存表单信息
        $scope.save = function () {
            if (isBusy) {
                return false;
            }
            isBusy = true;
            if (!validateBaseInfo()) {
                isBusy = false;
                return false;
            }
            if (isUpdate) {
                disasterManageService.update($scope.disasterPoint, function (data) {
                    saveCallback(data);
                    isBusy = false;
                });
            } else {
                disasterManageService.add($scope.disasterPoint, function (data) {
                    saveCallback(data);
                    isBusy = false;
                });
            }
        };
    });
});