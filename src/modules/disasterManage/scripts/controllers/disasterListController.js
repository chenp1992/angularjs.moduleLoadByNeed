define([
    "disasterManageModule",
    "disasterManage/scripts/services/disasterManageService",
    "disasterManage/scripts/controllers/disasterPointController"
], function (disasterManageModule) {
    "use strict";

    disasterManageModule.controller("disasterListController", function ($scope, $q, modal, messager, disasterManageService) {

        $scope.enumData = {
            "disasterTypes": [{
                    "value": 0,
                    "text": "全部"
                },
                {
                    "value": 1,
                    "text": "滑坡"
                },
                {
                    "value": 2,
                    "text": "泥石流"
                },
                {
                    "value": 3,
                    "text": "崩塌"
                }
            ],
            "stabilitys": [{
                    "value": 0,
                    "text": "全部"
                },
                {
                    "value": 1,
                    "text": "低易发"
                },
                {
                    "value": 2,
                    "text": "中易发"
                },
                {
                    "value": 3,
                    "text": "较差"
                },
                {
                    "value": 4,
                    "text": "差"
                }
            ],
            "harmDegrees": [{
                    "value": 0,
                    "text": "全部"
                },
                {
                    "value": 1,
                    "text": "一般级"
                },
                {
                    "value": 2,
                    "text": "较大级"
                },
                {
                    "value": 3,
                    "text": "重大级"
                }
            ],
            "scaleLevels": [{
                    "value": 0,
                    "text": "全部"
                }, {
                    "value": 1,
                    "text": "小型"
                },
                {
                    "value": 2,
                    "text": "中型"
                },
                {
                    "value": 3,
                    "text": "大型"
                }
            ],
            "dangerTypes": [{
                    "value": 0,
                    "text": "全部"
                }, {
                    "value": 1,
                    "text": "小"
                },
                {
                    "value": 2,
                    "text": "中等"
                },
                {
                    "value": 3,
                    "text": "大"
                }
            ],
            "disasterTargets": [{
                    "value": 0,
                    "text": "全部"
                }, {
                    "value": 1,
                    "text": "居民房屋"
                },
                {
                    "value": 2,
                    "text": "教育设施"
                },
                {
                    "value": 3,
                    "text": "公路交通"
                },
                {
                    "value": 4,
                    "text": "工业厂房"
                }, {
                    "value": 5,
                    "text": "宗教场所"
                },
                {
                    "value": 6,
                    "text": "活动场所"
                },
                {
                    "value": 7,
                    "text": "宾馆酒店"
                },
                {
                    "value": 8,
                    "text": "旅游景点"
                },
                {
                    "value": 9,
                    "text": "其它设施"
                },
                {
                    "value": 10,
                    "text": "居民房屋+教育设施"
                }
            ]
        };

        $scope.queryResult = {
            "pageCount": 0,
            "pageNo": 1,
            "pageSize": 20,
            "recordCount": 0,
            "maxSize": 10,
            "currentList": []
        };

        $scope.queryParam = {
            "disasterType": 0,
            "harmDegree": 0,
            "stability": 0,
            "location": "",
            "maxScale": 100,
            "minScale": 0,
            "no": "",
            "townStreet": ""
        };

        //标识是否正在进行操作
        var isBusy = false;

        function queryData() {
            var deferred = $q.defer();
            var queryParam = {
                "pageNo": $scope.queryResult.pageNo,
                "pageSize": $scope.queryResult.pageSize,
                "paramer": $scope.queryParam
            };
            //请求数据
            disasterManageService.queryList(queryParam, function (data) {
                if (data && data.resultStatus === "SUCCESS") {
                    deferred.resolve(data.data);
                } else {
                    deferred.reject(data.message);
                }
            });

            return deferred.promise;
        }

        //查询地灾点列表
        $scope.query = function (pageNo) {
            if (pageNo) {
                $scope.queryResult.pageNo = pageNo;
            }
            //加载数据
            if (isBusy) {
                return false;
            }
            isBusy = true;
            queryData().then(function (data) {
                if (data) {
                    $scope.queryResult.recordCount = data.recordCount;
                    $scope.queryResult.currentList = data.currentList;
                    if (data.currentList && data.currentList.length > 0) {
                        angular.forEach(data.currentList, function (item) {
                            //类型
                            var disasterType = $scope.enumData.disasterTypes.find(function (o) {
                                return o.value === item.disasterType;
                            });
                            item.disasterTypeText = disasterType ? disasterType.text : "";
                            //稳定性
                            var stability = $scope.enumData.stabilitys.find(function (o) {
                                return o.value === item.stabilityType;
                            });
                            item.stabilityText = stability ? stability.text : "";
                            //危害程度
                            var harmDegree = $scope.enumData.harmDegrees.find(function (o) {
                                return o.value === item.harmDegree;
                            });
                            item.harmDegreeText = harmDegree ? harmDegree.text : "";
                            //规模等级
                            var scaleLevel = $scope.enumData.scaleLevels.find(function (o) {
                                return o.value === item.scaleLevel;
                            });
                            item.scaleLevelText = scaleLevel ? scaleLevel.text : "";
                            //受灾对象
                            var disasterTarget = $scope.enumData.disasterTargets.find(function (o) {
                                return o.value === item.affectedObject;
                            });
                            item.disasterTargetText = disasterTarget ? disasterTarget.text : "";
                        });
                    }
                }
                isBusy = false;
            }, function () {
                isBusy = false;
            });
        };

        //重置查询条件
        $scope.reset = function () {
            //重置所有查询选项
            $scope.queryParam.disasterType = 0;
            $scope.queryParam.harmDegree = 0;
            $scope.queryParam.stability = 0;
            $scope.queryParam.maxScale = 100;
            $scope.queryParam.minScale = 0;
            $scope.queryParam.location = "";
            $scope.queryParam.townStreet = "";
            $scope.queryParam.no = "";
        };

        //添加地灾隐患点
        $scope.addDisasterPoint = function () {
            modal.open("/modules/disasterManage/views/disasterPoint.html", 1050, "disasterPointController", {
                    values: null
                })
                .result.then(function () {
                    $scope.query(1);
                });
        };

        //编辑地灾隐患点
        $scope.editDisasterPoint = function (id) {
            modal.open("/modules/disasterManage/views/disasterPoint.html", 1050, "disasterPointController", {
                values: {
                    id: id
                }
            }).result.then(function () {
                $scope.query(1);
            });
        };

        //删除
        $scope.delete = function (item) {
            if (item && item.id) {
                if (isBusy) {
                    return false;
                }
                isBusy = true;
                messager.confirm("是否确定删除该记录？", function () {
                    disasterManageService.delete(item.id, function (data) {
                        if (data && data.resultStatus === "SUCCESS") {
                            messager.alert("删除成功！", 1800);
                            $scope.query(1);
                        } else {
                            messager.alert("删除失败：" + data.message + "！");
                        }
                        isBusy = false;
                    });
                }, function () {
                    isBusy = false;
                });
            }
        };

        //查询列表数据
        $scope.query(1);
    });
});