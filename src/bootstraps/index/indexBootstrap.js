require([
    "indexModule",
    "bootstraps/index/indexController"
], function () {

    "use strict";

    angular.bootstrap(document, ["indexModule"]);
});