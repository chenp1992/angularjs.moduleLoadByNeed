define([
    "moment",
    "infrastructureRegister"
], function (moment) {

    "use strict";

    moment.locale("zh-cn");

    //主模块
    var indexModule = angular.module("indexModule", ["infrastructureModule", "pasvaz.bindonce", "ngRoute", "ngCookies", "ngMessages",
        "ngAnimate", "ngLocale", "angularFileUpload", "ui.router", "ui.bootstrap", "oc.lazyLoad"
    ]);

    indexModule.config(["$ocLazyLoadProvider", "$stateProvider", "$routeProvider", '$urlRouterProvider', function ($ocLazyLoadProvider, $stateProvider, $routeProvider, $urlRouterProvider) {
        
        //延迟加载模块配置
        $ocLazyLoadProvider.config({
            debug: false,
            modules: [{
                name: "disasterManageModule",
                files: [
                    "../../modules/disasterManage/styles/disasterManage.min.css",
                    "disasterManageRegister"
                ]
            }]
        });

        var deferred;

        function resolveDeps(module) {
            return {
                loadModule: ['$ocLazyLoad', '$q', function ($ocLazyLoad, $q) {
                    deferred = $q.defer();
                    return $ocLazyLoad.load([
                        module
                    ]).then(function () {
                        deferred.resolve();
                    });
                }]
            };
        }


        $urlRouterProvider.otherwise("disasterManage");

        //页面路由配置
        $stateProvider
            .state("disasterManage", {
                url: "/disasterManage",
                views: {
                    "main": {
                        templateUrl: "../../modules/disasterManage/views/disasterList.html",
                        controller: "disasterListController"
                    }
                },
                resolve: resolveDeps("disasterManageModule")
            });
    }]);

    /* indexModule.run(function (datepickerConfig, datepickerPopupConfig) {
        datepickerConfig.showWeeks = false;
        datepickerPopupConfig.showButtonBar = true;
        datepickerPopupConfig.currentText = "今天";
        datepickerPopupConfig.clearText = "清除";
        datepickerPopupConfig.closeText = "关闭";
    }); */

    return indexModule;
});