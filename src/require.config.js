require.config({
    baseUrl: '/',
    paths: {

        // 文件夹
        'libs': '/libs',
        'bootstraps': '/bootstraps',
        'modules': '/modules',
        'common': '/modules/common',
        'infrastructure': '/modules/infrastructure',
        'disasterManage': '/modules/disasterManage',

        'indexModule': '/bootstraps/index/indexModule',
        'infrastructureModule': '/modules/infrastructure/scripts/infrastructureModule',
        'disasterManageModule': '/modules/disasterManage/scripts/disasterManageModule',

        'infrastructureRegister': '/modules/infrastructure/scripts/infrastructureRegister',
        'disasterManageRegister': '/modules/disasterManage/scripts/disasterManageRegister',

        //第三方库
        'jquery': '/libs/jquery/jquery-1.11.1.min',
        //'uiBootstrap': '/libs/uiBootstrap/ui-bootstrap-tpls',
        'uiBootstrap': '/libs/uiBootstrap/ui-bootstrap-tpls-0.13.1',
        'moment': '/libs/moment/moment-with-locales.min',
        'ocLazyLoad': '/libs/ocLazyLoad/ocLazyLoad.require',

        //angularjs
        'angular': '/libs/angularjs/angular',
        'angular-cookies': '/libs/angularjs/angular-cookies.min',
        'angular-route': '/libs/angularjs/angular-route.min',
        'angular-animate': '/libs/angularjs/angular-animate.min',
        'angular-locale_zh-cn': '/libs/angularjs/i18n/angular-locale_zh-cn',
        'angular-messages': '/libs/angularjs/angular-messages.min',
        'angular-sanitize': '/libs/angularjs/angular-sanitize.min',
        'angular-ui-tree': '/libs/angularjs/angular-ui-tree.min',
        'angular-file-upload': '/libs/angularjs/angular-file-upload.min',
        'angular-ui-router': '/libs/angularjs/angular-ui-router',
        'bindonce': '/libs/angularjs/bindonce',

        //配置文件
        'config': '/web.cfg',
    },
    map: {
        '*': {
            'css': 'lib/css'
        }
    },
    shim: {
        'jquery': {
            exports: 'jquery'
        },
        'angular': {
            deps: ['jquery'],
            exports: 'angular'
        },
        'angular-sanitize': ['angular'],
        'angular-cookies': ['angular'],
        'angular-route': ['angular'],
        'angular-animate': ['angular'],
        'angular-ui-tree': ['angular'],
        'angular-locale_zh-cn': ['angular'],
        'angular-messages': ['angular'],
        'angular-file-upload': ['angular'],
        'angular-ui-router': ['angular'],
        'angular-tooltips': ['angular'],
        'bindonce': ['angular'],
        'indexModule': ['angular', 'bindonce', 'jquery', 'angular-cookies', 'angular-sanitize', 'angular-messages', 'angular-route', 'angular-animate',
            'angular-locale_zh-cn', 'angular-ui-tree', 'angular-file-upload', 'angular-ui-router', 'uiBootstrap', 'ocLazyLoad'
        ],
        'infrastructureModule': ['angular'],
        'uiBootstrap': ['angular'],
        'ocLazyLoad': ['angular']
    }
});